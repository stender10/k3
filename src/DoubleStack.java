import java.util.LinkedList;

public class DoubleStack {

   private LinkedList<Double> linkedStack;

   public static void main (String[] argum) {
      // TODO!!! Your tests here!
      DoubleStack newStack = new DoubleStack();
      newStack.push(3.5);
      newStack.push(2.5);
      newStack.push(4.5);
      newStack.pop();
      newStack.tos();

      System.out.println(newStack);
      System.out.println("Size: " + newStack.linkedStack.size());
      System.out.println(newStack.tos());

      System.out.println(DoubleStack.interpret("3 2 + 2 *"));
      System.out.println(DoubleStack.interpret("\t2. \t5. +   \t"));
      System.out.println(DoubleStack.interpret("2 5 SWAP -"));
      System.out.println(DoubleStack.interpret("2 5 9 ROT - +"));
      System.out.println(DoubleStack.interpret("2 5 9 ROT + SWAP -"));
//      // Check for empty expression
//      System.out.println(DoubleStack.interpret(" "));
//      // Check for expression with too few numbers
//      System.out.println(DoubleStack.interpret("3 - 3"));
//      // Check for expression with illegal symbol
//      System.out.println(DoubleStack.interpret("3 2 y 2 *"));
   }

   DoubleStack() {
      // TODO!!! Your constructor here!
      linkedStack = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      // TODO!!! Your code here!
      DoubleStack clone = new DoubleStack();

      for (int i = 0; i < linkedStack.size(); i++) {
         clone.push(linkedStack.get(i));
      }

      return clone;
   }

   public boolean stEmpty() {
      // TODO!!! Your code here!
      return linkedStack.isEmpty();
   }

   public void push (double a) {
      // TODO!!! Your code here!
      linkedStack.addLast(a);
   }

   public double pop() {
      // TODO!!! Your code here!
      if (stEmpty()) throw new RuntimeException("Stack underflow!");

      return linkedStack.removeLast();
   }

   public void op (String s) {
      // TODO!!!
      if (linkedStack.size() < 2)
         throw new RuntimeException("You need at least " +
                 "two elements in stack to make an operation!");

      double op2 = pop();
      double op1 = pop();
      if (s.equals("+")) {
         push(op1 + op2);
      } else if (s.equals("-")) {
         push(op1 - op2);
      } else if (s.equals("*")) {
         push(op1 * op2);
      } else if (s.equals("/")) {
         push(op1 / op2);
      } else if (s.equals("SWAP")) {
         push(op2);
         push(op1);
      } else if (s.equals("ROT")) {
         if (linkedStack.size() < 1) throw new RuntimeException("You need at least " +
                 "three elements in stack to make an ROT operation!");
         double op3 = pop();
         push(op1);
         push(op2);
         push(op3);
      }

      else {
         throw new IllegalArgumentException(s + " is invalid argument!");
      }
   }
  
   public double tos() {
      // TODO!!! Your code here!
      if (stEmpty()) throw new IndexOutOfBoundsException("Stack underflow!");
      return linkedStack.getLast();
   }

   @Override
   public boolean equals (Object o) {
      // TODO!!! Your code here!
      if (o == this) return true;

      if (!(o instanceof DoubleStack)) return false;

      DoubleStack c = (DoubleStack) o;

      if (linkedStack.size() != c.linkedStack.size()) return false;

      for (int i = 0; i < linkedStack.size(); i++) {
         if (!linkedStack.get(i).equals(c.linkedStack.get(i))) return false;
      }

      return true;
   }

   @Override
   public String toString() {
      // TODO!!! Your code here!
      if (stEmpty()) {
         return "Stack is empty!";
      }

      StringBuilder result = new StringBuilder();

      for (int i = 0; i < linkedStack.size(); i++) {
         result.append(linkedStack.get(i)).append("\n");
      }

      return result.toString();
   }

   public static double interpret (String pol) {
      DoubleStack linkedStack = new DoubleStack();
      String operators = "+-*/SWAPROT";
      String[] expression = pol.trim().split("\\s+");

      if (expression.length == 0)
         throw new RuntimeException("Invalid expression: " + pol + "\n"
         + "Empty expression.");

      for (String e : expression) {

         if (operators.contains(e)) {
            linkedStack.op(e);
         } else
            try {
               double d = Double.parseDouble(e);
               linkedStack.push(d);
            } catch (NumberFormatException | NullPointerException nfe) {
               throw new RuntimeException("Invalid expression: " + pol + "\n"
                       + "Invalid symbol: " + e);
            }

      }

      if (linkedStack.linkedStack.size() > 1)
         throw new RuntimeException("Invalid expression: " + pol + "\n"
              + "Too many numbers in expression.");

      return linkedStack.tos();
   }
}
